function f=ExternalConstraints(t,f,nmodes,flag)
%
% Generic external constraints function.
%
% Input: flag - 0 = initialization call; 1= run time simulation call;
%                   2=post-processing shut-down call
%        t                    - current time
%        f(1:nmodes)          - body velocities in modes 1:nmodes
%        f(nmodes+1:2*nmodes) - body displacement in modes 1:nmodes
%        nmodes               - number of degrees of freedom
%
% Output: f(1:2*nmodes,1) - Position and velocities after applying
%                           constraints.
% 
% If this routine needs to save variables, then declare them here using
% persistent, i.e.: 
%
% persistent U
%
switch flag
    case 0
        %
        % Initialization call for set up.
        %
        display('Dummy external constraints file, no constraints applied.')
    case 1
        %
        % Run time call
        %
    case 2
        %
        % Final call for post-processing.
        %
        display('Dummy external constraints file, nothing to do here.')
    otherwise
        error('Called ExternalConstraints with flag/= 0,1,2')
end
