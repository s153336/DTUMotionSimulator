%
%
%  ReadInput
%  
% This script reads in all the data needed to start the simulation.  
% Data can be either from WAMIT or TiMIT at this point.  
%
%%
%
% Read in, or compute, the body geometry the inertia, the hydrostatic and 
% the hydrodynamic coefficients.  
%
switch FRFtype
    case 'WAMIT'
        %
        % We assume here that the WAMIT run has been made solving the
        % radition problem ONLY, and at a uniform frequency spacing from
        % zero to f_max, plus f=infinity as the last value.  Thus the 
        % WAMIT parameters IPERIN=2 and IPEROUT=1 (IPERIO=2 in pre-version
        % 7 versions) should be set in the config file(s), and the 
        % frequencies should begin with -1 and end with 0, in the .pot file.  
        %
        % Read and possibly plot the geometry in the .gdf file.
        %
        [ulen,grav]=textread(GDFfname,'%f %f %*s',1,'headerlines',1);
        %
        % Non-dimensionalize the water depth
        %
        hbot=hbot/ulen;
        [is(1,1),is(2,1)]=textread(GDFfname,'%d %d %*s',1,'headerlines',2);
        %
        switch gdfOrder
            case 'Low'
                %
                % For low-order gdfs, read and plot up the panels
                %
                [np]=textread(GDFfname,'%d %*s',1,'headerlines',3);
                [xp,yp,zp]=textread(GDFfname,'%f %f %f ',...
                    'headerlines',4);
                %
                figure(1); clf; hold on;
                for k=1:4:4*np
                    plot3(xp([k:k+3 k],1),yp([k:k+3 k],1),zp([k:k+3 k],1),'k')
                end
                AZ=40;
                EL=20;
                view(AZ,EL)
                xlabel('x')
                ylabel('y')
                zlabel('z')
                axis('equal'); hold off;
        end
        %        
        % Read in the added mass and damping coefficients
        %
        Order='TM';
        [T,A,B]=ReadWAMIT1file(strcat(FRFfname,'.1'),nHead,Order);
        nf=length(T);
        nmodesA=size(A,2);  % The logical size of the hydrodynamic coefficients
        %
        % The total number of degrees of freedom, including possible
        % generalized modes and all multiple bodies. 
        %
        nmodes=length(imode);  
        for m=1:nmodes
            if imode(m)==0, 
                FixedModes=[FixedModes m]; 
            else
                FreeModes=[FreeModes m];
            end
        end
        %
        if nmodes ~= nmodesA
            warning(['Your WAMIT coefficients have logical size ',num2str(nmodesA),...
                ' while you have input ',num2str(nmodes),...
                ' total modes. The system will truncated to ',num2str(nmodesA),...
                ' after reading the mass and stiffness matrices.']);
        end
        %
        % Check that WAMIT has been run properly for getting the Fourier
        % Transform and build the radiation Fourier space.
        %
        ifinf=find(T==0); if0=find(T<0); 
        if exist('if0') ~=1 | if0 ~=1, error('Your frequency range should start with omega=0');end
        if exist('ifinf') ~=1, error('omega=infinity was not found, add this value and re-run WAMIT');end
        %
        % Save the infinite frequency added mass matrix and remove this
        % value from T, A and B.
        %
        ajk(:,:)=A(ifinf,:,:); 
        T(ifinf,:)=[]; A(ifinf,:,:)=[]; B(ifinf,:,:)=[];
        nf=nf-1;
        %
        % Set the omega vector.
        %
        switch IPEROUT
            case 1
                om=2*pi./T; om(1,1)=0; 
            case 2
                om=T; om(1,1)=0;
        end
        %
        % Build the non-dimensional frequency and time spaces
        %
        ffac=sqrt(ulen/grav); freq=ffac.*om/(2*pi);
        maxfrq=freq(nf,1); f1=ffac*om(2,1)/(2*pi); dltfrq=maxfrq/(nf-1);
        if abs(dltfrq-f1)/maxfrq>10^-5
            error('Your frequency range does not appear to be uniformly spaced');
        end
        %
        disp(['Found ',num2str(nf),' frequencies (plus infinity) in the .1 file.']);
        disp(['WAMIT coeff.s are evenly spaced in the range 0<omega<',...
            num2str(2*pi*maxfrq/ffac), ' with domega=',num2str(2*pi*dltfrq/ffac)]);
        irlim=nf; dt_irf=1/(2*maxfrq);
        %
        % WAMIT output is B/(rho L^k omega), convert to 
        % B/(rho L^k sqrt(L/g)) for the FFT.
        %
        for j=1:nmodesA
            for k=1:nmodesA
                B(:,j,k)=ffac*om.*B(:,j,k);
            end
        end
        %
        % Reverse the sign of the added mass on the Free Surface Pressure 
        % modes to take into account the WAMIT BC of +1 which should be -1
        % theory.  
        %
        for m=FSPmodes
            A(:,m,:)=-A(:,m,:); 
        end
        %
        % Initialize the external damping coefficients.
        %
        bjk=zeros(nmodes,nmodes);
        %
        % Fourier transform the damping coefficients to get the IRFs.
        % I use the FFT here since DCT-I is not available in Matlab and I 
        % prefer this definition of the DCT to  DCT-II/III.
        %
        Kjk_W=zeros(nf,nmodesA,nmodesA);
        for k=1:nmodesA
            for j=1:nmodesA
                % Build the whole frequency coeff. range for + and - omega
                c=B(:,j,k); c(nf+1:2*nf-2,1)=conj(c(nf-1:-1:2,1)); 
                c=2/dt_irf*ifft(c,'symmetric');
                Kjk_W(:,j,k)=c(1:nf,1); 
            end
        end
        %
        % Read the Haskind or Diffraction exciting forces.
        %
        clear T;
        if exist(strcat(FRFfname,'.2'),'file')
            [beta,T,XMag,XPhase,XR,XI]=ReadWAMIT234file(strcat(FRFfname,'.2'),...
                nHead,Order);
        elseif exist(strcat(FRFfname,'.3'),'file')
            [beta,T,XMag,XPhase,XR,XI]=ReadWAMIT234file(strcat(FRFfname,'.3'),...
                nHead,Order);
        else
            error('No diffraction file found.')
        end
        %
        % Check the modes and angles
        %
        nbeta=length(beta); nmodesH=size(XR,2); modesH=[1:nmodesH]; 
        nper=size(XR,1); 
        if nper-nf+1~=0, error('Your .1 and .2 files do not match.'); end
        if nmodesH-nmodesA~=0, error('Your .1 and .2 files do not match.'); end
        disp(['Found ',num2str(nbeta),' heading angles and ',num2str(nper),...
            ' periods in the .2 file.']);
        %
        % Load the exciting force coefficient matrix including the zero
        % frequency value. These values are all zero except for in heave 
        % where it should be the hydrostatic restoring force coefficient 
        % which is inserted after it is read.  
        %
        XjD=zeros(nf,nmodesA,nbeta);
        for ib=1:nbeta
            for m=1:nmodesA
                XjD(2:nf,m,ib)=complex(XR(:,m,ib),XI(:,m,ib));
            end
        end
        %
        % Read in the inertia, and possibly the external damping and 
        % stiffnes matrices from the .mmx file and the hydrostatic matrix 
        % from the .hst file.  
        %
        % The hydrostatic matrix is already non-dimensional.
        %
        fid=fopen(strcat(FRFfname,'.hst')); clear tmp;
        for j=1:nHead, fgetl(fid); end % Read 1 header line
        [tmp]=fscanf(fid,'%f',3*nmodes^2); 
        cjk=reshape(tmp(3:3:end),nmodes,nmodes);
        fclose(fid);
        %
        % The zero-frequency limit of the heave exciting force.  
        XjD(1,3,:)=cjk(3,3);
        %
        % 
        if ialtfrc == 1
            %
            % Read only M_ij/rho for 6 rigid-body degrees of freedom.  
            %
            fid=fopen(strcat(FRFfname,'.mmx')); clear tmp;
            for j=1:12+nHead, fgetl(fid); end;
            [tmp]=fscanf(fid,'%f',3*nmodes^2);
            fclose(fid);
            msmtrx=reshape(tmp(3:3:end),nmodes,nmodes);
            % Non-dimensionalize the mass matrix.  
            msmtrx=msmtrx/ulen^3;
            msmtrx(1:3,4:6)=msmtrx(1:3,4:6)/ulen; 
            msmtrx(4:6,1:3)=msmtrx(4:6,1:3)/ulen;
            msmtrx(4:6,4:6)=msmtrx(4:6,4:6)/ulen^2;
        elseif ialtfrc == 2
            %
            % Read the dimensional M_ij B_ij and C_ij.  Add the external 
            % C_jk to the hydrostatic C_jk.  In this case, we assume that
            % ulen=1.  
            %
            warning(['You have used ialtfrc>1 for this run',...
                ' so if there are generalized and/or pressure modes ',...
                ' then ulen may need to be set to 1.0']);
            fid=fopen(strcat(FRFfname,'.mmx')); clear tmp;
            for j=1:nHead+5+nbody*5+2, fgetl(fid); end;
            [tmp]=fscanf(fid,'%f',5*nmodes^2);
            fclose(fid); 
            msmtrx=reshape(tmp(3:5:end),nmodes,nmodes)/rho;
            bjk=reshape(tmp(4:5:end),nmodes,nmodes)/(rho*sqrt(grav/ulen));
            cjk2=(reshape(tmp(5:5:end),nmodes,nmodes)/(rho*grav));
            %
            % Non-dimensionalize the mass, damping and stiffness matrices.
            % Here generalized and/or pressure modes are
            % non-dimensionalized as translation modes, so if these 
            % matrices is important then we expect ulen=1.  
            %
            msmtrx=msmtrx/ulen^3;
            msmtrx(1:3,4:6)=msmtrx(1:3,4:6)/ulen; 
            msmtrx(4:6,1:3)=msmtrx(4:6,1:3)/ulen;
            msmtrx(4:6,4:6)=msmtrx(4:6,4:6)/ulen^2;
            bjk=bjk/ulen^3;
            bjk(1:3,4:6)=bjk(1:3,4:6)/ulen; 
            bjk(4:6,1:3)=bjk(4:6,1:3)/ulen;
            bjk(4:6,4:6)=bjk(4:6,4:6)/ulen^2;
            cjk2=cjk2/ulen^2;
            cjk2(1:3,4:6)=cjk2(1:3,4:6)/ulen; 
            cjk2(4:6,1:3)=cjk2(4:6,1:3)/ulen;
            cjk2(4:6,4:6)=cjk2(4:6,4:6)/ulen^2;
            %
            % Add the external stiffness to the hydrostatic matrix
            %
            cjk=cjk+cjk2;
            %
            % If the logical sizes of the hydrodynamic and inertia
            % (stiffness, external damping) matrices are different, 
            % them to the hydrodynamic size. 
            %
            if nmodesA ~= nmodes
                warning(['Truncating the system size to ',num2str(nmodesA),' degrees of freedom.']);
                msmtrx(nmodesA+1:nmodes,:)=[]; msmtrx(:,nmodesA+1:nmodes)=[]; 
                bjk(nmodesA+1:nmodes,:)=[]; bjk(:,nmodesA+1:nmodes)=[]; 
                cjk(nmodesA+1:nmodes,:)=[]; cjk(:,nmodesA+1:nmodes)=[]; 
                nmodes=nmodesA;
                tmp=find(FixedModes>nmodes); FixedModes(tmp)=[];
             end
        else
            error('ialtfrc must be either 1 or 2');
        end
        %
        % Check for static stability
        %
        unstableModes=[];
        for j=1:nmodes
            if cjk(j,j)<0, unstableModes=[unstableModes j]; end
        end
        unstableN=length(unstableModes);
        if unstableN~=0,
            warning(['Mode(s): ',num2str(unstableModes),' are statically unstable.']);
            UnstableFreeModes=intersect(FreeModes,unstableModes);
            if length(UnstableFreeModes)~=0,
                if min(UnstableFreeModes)<7 
                    error('A free rigid-body mode is unstable.'); 
                else
                    warning('A generalized mode has a negative hydrostatic coefficient.');
                end;
            end;
        end
        %
        % Add the inertia and the infinite frequency added-mass and check 
        % for negative mass.  invert the complete inertia matrix.  
        %
        minv=msmtrx+ajk; 
        unstableMass=[];
        for j=1:nmodes
            if minv(j,j)<=0, unstableMass=[unstableMass j]; end
        end
        unstableN=length(unstableMass);
        if unstableN~=0,
            warning(['Mode(s): ',num2str(unstableMass),...
                ' have zero or negative impulsive mass and are hence unstable.']);
            if intersect(FreeModes,unstableMass)~=0, 
                error('A free mode is dynamically unstable with M(j,j)+a(j,j)<=0.'); 
            end;
        end
        %
        % Remove the influence of the fixed modes from the mass matrix
        %
        for m=FixedModes
            minv(m,:)=0; minv(:,m)=0; minv(m,m)=1;
        end
        if det(minv)==0, error('Mass matrix is singular.'); end
        %
        minv=inv(minv);
        %
    case 'TiMIT'
        %
        % Read in the geometric, static and IRF/FRF data computed by TiMIT
        %
        fnames=textread('FNAMES.TD','%q');
        %
        % Read the pcf
        %
        [ipot,isor,ivel,igreen,U,hbot]= ...
            textread(fnames{1,1},'%u %u %u %u %f %f %*s',1,'headerlines',1);
        [nbody]=textread(fnames{1,1},'%u %*s',1,'headerlines',2);
        fnames(3)=textread(fnames{1,1},'%s ',1,'headerlines',3);
        [xbody(1),xbody(2),xbody(3),xbody(4)]= ...
            textread(fnames{1,1},'%f %f %f %f %*s',1,'headerlines',4);
        [irad idif]=textread(fnames{1,1},'%d %d %*s',1,'headerlines',5);
        [imode(1),imode(2),imode(3),imode(4),imode(5),imode(6),genmod]=...
            textread(fnames{1,1},'%u %u %u %u %u %u %u %*s',1,'headerlines',6);
        [dt_irf,trad,tdmin,tdmax]= ...
            textread(fnames{1,1},'%f %f %f %f %*s',1,'headerlines',7);
        [nbeta]=textread(fnames{1,1},'%u %*s',1,'headerlines',8);
        for ib=1:nbeta
            [beta(ib)]=textread(fnames{1,1},'%f %*s',1,'headerlines',8+ib);
        end
        if irad==1,imode=ones(1,6);end
        % Fixed and free rigid-body modes
        for m=1:6
            if imode(m)==0, 
                FixedModes=[FixedModes m]; 
            else
                FreeModes=[FreeModes m];
            end
        end
        %
        % Read and plot the geometry
        %
        [ulen,grav]=textread(fnames{3,1},'%f %f %*s',1,'headerlines',1);
        [is(1,1),is(2,1)]=textread(fnames{3,1},'%d %d %*s',1,'headerlines',2);
        [np]=textread(fnames{3,1},'%d %*s',1,'headerlines',3);
        [xp,yp,zp]=textread(fnames{3,1},'%f %f %f ',...
            'headerlines',4);
        %
        figure(1); clf;  hold on;
        for k=1:4:4*np
            plot3(xp([k:k+3 k],1),yp([k:k+3 k],1),zp([k:k+3 k],1),'k')
            plot3(xp([k:k+3 k],1),-yp([k:k+3 k],1),zp([k:k+3 k],1),'k')
        end
        AZ=40;
        EL=20;
        view(AZ,EL)
        xlabel('x')
        ylabel('y')
        zlabel('z')
        axis('equal', 'off'); hold off;
        %   
        % Non-dimensionalize the depth, speed, xbody and the IRF time.  
        hbot=hbot/ulen; xbody=xbody/ulen; U=U/sqrt(grav*ulen); 
        tfac=sqrt(grav/ulen); dt_irf=dt_irf*tfac; trad=trad*tfac; 
        tdmin=tdmin*tfac; tdmax=tdmax*tfac; 
        irlim=trad/dt_irf+1;
        % 
        % Read the rcf frequency range as a check.  
        %
        [dltfrq, fmax]=textread(fnames{2,1},'%f %f ',1,'headerlines',3);
        nper=fmax/dltfrq; 
        %
        % Read the added mass and damping coefficient file to work out the
        % mode combinations.  
        %
        [Traw,mode1,mode2,Araw,Braw]= textread('ifoptn.1','%f %d %d %f %f ',...
            'headerlines',4);
        tmp=unique(Traw); nf=length(tmp); ncomb=length(Araw)/nf; 
        if nf~=nper
            warning([num2str(nf),' frequencies found in the ifoptn.1 file but ',...
                num2str(nper),' frequences were found in the .rcf file.']);
        end
        %
        % Non-dimensionalize the frequency-space (cyclic frequency).
        %
        ffac=sqrt(ulen/grav); dltfrq=ffac*dltfrq/(2*pi); 
        fmax=ffac*fmax/(2*pi);
        %
        % The total number of degrees of freedom, including possible
        % generalized modes. Generalized modes are always considered free.
        %
        nmodes=6;
        if max(mode1)>6
            newmds=max(mode1)-6; nmodes=nmodes+newmds;
            FreeModes=[FreeModes 6+[1:newmds]];
        end
        %
        Kjk_W=zeros(irlim,nmodes,nmodes); ajk=zeros(nmodes,nmodes);
        bjk=zeros(nmodes,nmodes); cjk0=zeros(nmodes,nmodes);
        %
        % Read in the non-zero radiation IRFs
        %
        for id=1:ncomb
            m(id)=mode1((id-1)*nper+1); n(id)=mode2((id-1)*nper+1); 
            fname=strcat('irf.0',num2str(m(id)),'0',num2str(n(id)));
            [ajk(m(id),n(id)),bjk(m(id),n(id)),cjk0(m(id),n(id))] = ...
                textread(fname,'%f %f %f',1,'headerlines',5);
            [dum,Kjk_W(:,m(id),n(id))]=textread(fname,'%f %f','headerlines',6);
        end
        %
        % Read the Diffraction exciting forces.
        %
        clear T;
        [T,beta,mode,XDmagRaw,XDphaseRaw,XDreal,XDimag]= ...
            textread('ifoptn.3','%f %f %d %f %f %f %f','headerlines',4);
        %
        % Figure out how many heading angles we have 
        %
        beta=unique(beta); nbeta=length(beta);
        %
        stride=length(FreeModes)*nbeta; nper=length(XDmagRaw)/stride; 
        if nper-nf~=0, error('Your .1 and .3 files do not match.'); end
        disp(['Found ',num2str(nbeta),' heading angles and ',num2str(nper),...
            ' periods in the TiMIT .3 file.']);
        %
        % Re-sort the coefficients by mode and heading.
        %
        XjD=zeros(nf,nmodes,nbeta);
        for ib=1:nbeta
            for m=1:length(FreeModes)
                i0=(ib-1+m-1)*nper+1; mHask(m)=mode(i0);
                XjD(1:nper,mHask(m),ib)=XDreal(i0:i0+nper-1,1) ...
                    + 1i * XDimag(i0:i0+nper-1,1);
            end
        end
        %
        % Read in the non-dimensional inertia and hydrostatic matrices
        % from the file 'static.dat'
        %
        nm=max(6,nmodes);
        fid=fopen('static.dat'); clear tmp;
        for j=1:5, fgetl(fid); end % Read 5 header lines
        [tmp]=fscanf(fid,'%f',nm^2); cjk=reshape(tmp,nm,nm)+cjk0;
        for j=1:2, fgetl(fid); end;
        [tmp]=fscanf(fid,'%f',nm^2); msmtrx=reshape(tmp,nm,nm);
        fclose(fid);
        %
        % Add the inertia and the infinite frequency added-mass and invert 
        % the complete inertia matrix.  
        %
        % Check for static and basic dynamic stability
        %
        unstableModes=[];
        for j=1:nmodes
            if cjk(j,j)<0, unstableModes=[unstableModes j]; end
        end
        unstableN=length(unstableModes);
        if unstableN~=0,
            error(['Mode(s): ',num2str(unstableModes),' are statically unstable.']);
        end
                
        minv=inv(msmtrx+ajk); 
        %
    otherwise
        error('Only WAMIT and TiMIT coefficients are supported at this point.');
end
%%
%
% Get the incident wave or decay test data.  
%
switch DecayTest
    case 'No'
        %
        % Read in the incident wave from the .iwf file.
        %
        fid=fopen(zeta0file); clear tmp;
        headerIWF=fgetl(fid);
        [tmp]=fscanf(fid,'%f %f %i',3); usim=tmp(1); dt_iwf=tmp(2); nbetaS=round(tmp(3));
        betaS=fscanf(fid,'%f',nbetaS);
        xwave=fscanf(fid,'%f',2);
        clear tmp; tmp=fscanf(fid,'%f');
        fclose(fid);
        nt_sim=length(tmp)/nbetaS;
        zeta=reshape(tmp,nt_sim,nbetaS);
        %
        % Non-dimensionalize the elevations and simulation time.
        %
        usim=usim/sqrt(grav*ulen);
        if abs(usim-U)>10^-6
            warning('Your forward speeds do not match');
        end
        dt_iwf=dt_iwf/sqrt(ulen/grav);
        dt_sim=dt_sim/sqrt(ulen/grav);
        zeta=zeta/ulen; xwave=xwave/ulen;
        if abs(dt_sim-dt_iwf)>10^-6
            warning(['Your .iwf and simulation time-steps do not agree, using dt_iwf=',...
                num2str(dt_iwf*sqrt(ulen/grav))]);
        end
        %
        % Build the time and frequency spaces for the incident wave.
        %
        dt_sim=dt_iwf;
        nft=floor((nt_sim)/2)+1; nt_sim=2*(nft-1); t_sim=[0:nt_sim-1]*dt_sim;
        zeta(nt_sim+1:end,:)=[];
        domega=2*pi/(nt_sim*dt_sim); omega=[0:nft-1]*domega;
        deg2rad=pi/180;
        %
        % Interpolate the diffraction FRFs onto the incident wave
        % Fourier space.
        %
        om_FRF=[0:nf-1]*dltfrq*2*pi;
        %
        % If neccessary, extend the frequency-space of the FRF assuming 
        % that it's reached a constant value. %hbb It should be zero right?
        %
        if omega(end)>om_FRF(end)
            nf2=ceil(omega(end)/(dltfrq*2*pi))+1; 
            om_FRF=[0:nf2-1]*dltfrq*2*pi;
            XjD(nf+1:nf2,:,:)=0;
        end
        % Interpolate
        for ib=1:nbetaS
            ibW=0;
            for ib2=1:nbeta
                if abs(beta(ib2)-betaS(ib))<10^-6
                    ibW=ib2; break
                end
            end
            if ibW==0
                error(['** No diffraction exciting force found at incident wave heading ',num2str(betaS(ib)),' **']);
            end
            for j=1:nmodes
                switch Interpolation
                    case 'linear'
                        XjD_iwf(:,j,ib)=interp1(om_FRF,XjD(:,j,ibW),omega,'linear','extrap');
                    case 'spline'
                        XjD_iwf(:,j,ib)=interp1(om_FRF,XjD(:,j,ibW),omega,'spline','extrap');
                    otherwise
                        error('Interpolation must be set to either linear or spline')
                end
            end
        end
        %
        % The headings angles are now consistent with the incident wave(s).
        %
        nbeta=nbetaS; beta=betaS;
        %
        % Interpolate the radiation IRFs onto the incident wave (simulation)
        % time-step size.
        %
        t_irfW=[0:irlim-1]*dt_irf; nt_irf=floor(t_irfW(end)/dt_sim)+1;
        t_irf=[0:nt_irf-1]*dt_sim;
        Kjk=zeros(nt_irf,nmodes,nmodes);
        for j=1:nmodes
            for k=1:nmodes
                switch Interpolation
                    case 'linear'
                        Kjk(:,j,k)=interp1(t_irfW,Kjk_W(:,j,k),t_irf,'linear','extrap');
                    case 'spline'
                        Kjk(:,j,k)=interp1(t_irfW,Kjk_W(:,j,k),t_irf,'spline','extrap');
                    otherwise
                        error('Interpolation must be set to either linear or spline')
                end
            end
        end
        %
        % For each incident wave heading angle, FFT the incident wave elevation,
        % move it/them to the body coordinate origin, multiply by the diffraction
        % FRF and accumulate the result in the frequency domain.  Then IFFT
        % to get the time-domain forcing.
        %
        FjD=zeros(nmodes,nt_sim);  % Initialize the diffraction forcing
        for ib=1:nbeta
            %
            % The Fourier coefficients of the elevation
            %
            cZeta=fft(zeta(1:nt_sim,ib));
            %
            % Compute the distance (dist) from the origin of the body-fixed coordina
            % to the point where the input wave elevation has been measured
            % along the direction of wave propogation.
            
            x1(1) = cos(deg2rad * beta(ib) );  x1(2) = sin(deg2rad * beta(ib) );
            x2(1) = xwave(1); x2(2) = xwave(2);
            
            % Theta is the angle between the (positive sense of the) incident wave v
            % and the (positive sense of the) measurement point vector.
            
            dist = x1(1) * x2(1) + x1(2) * x2(2);
            
            if abs(dist) > 1.e-6
                
                % If the measurement point is not at the origin, shift the incident wave
                % record to the origin.
                disp([' Shifting the incident wave record for beta=', ...
                    num2str(beta(ib)), ' from x=(',num2str(ulen*x2(1)),...
                    ',',num2str(ulen*x2(2)), ...
                    ' to the origin of the body fixed coordinates.']);
                error('Not yet implemented');
            end
            %
            % Multiply the exciting force and elevation coefficients and add it to
            % the frequency-domain wave forcing vector.   Note here that 
            % WAMIT defines F_jD(t) = Re{X_jD exp(i om t)} which corresponds to
            % matlab's IFFT.
            %
            clear c;
            for j=1:nmodes
                c(1:nft,1)=XjD_iwf(:,j,ib).*cZeta(1:nft,1); %The positive frequencies
                c(nft+1:2*(nft-1),1)=conj(c(nft-1:-1:2,1)); %The negative frequencies
                FjD(j,:)=FjD(j,:) + c.';   % Note .' here to indicate vector rather than conjugate transpose
            end
            
        end
        %
        % Inverse FFT to get the time-domain forcing
        %
        for j=1:nmodes
            FjD(j,:)=ifft(FjD(j,:),'symmetric');
        end
        %
        % The initial displacement of the body is zero.  
        %
        x0=zeros(nmodes,1);
        %
        %
        % figure; subplot(2,1,1); plot(omega,abs(XjD_iwf(:,3,1)));
        % subplot(2,1,2); plot(omega,atan2(imag(XjD_iwf(:,3,1)),real(XjD_iwf(:,3,1))));
        figure; plot(t_sim,zeta(:,1)); xlabel('t sqrt(g/L)'); ylabel('zeta/L');
        title('The incident wave signal');
        %
    case 'Yes'
        %
        % For a decay test, there is no forcing, just an initial
        % displacement of the body.  Non-dimensionalize the initial
        % positions.  
        disp('This run is a decay test with initial displacements:  ')
        x0
        deg2rad=pi/180;
        x0(1:3,1)=x0(1:3,1)/ulen; x0(4:6,1)=x0(4:6,1)*deg2rad;
        dt_sim=dt_sim/sqrt(ulen/grav);  % Non-dimensionalize the simulation time
        %
        %
        % The simulation time vector. 
        %
        t_sim=[0:nt_sim-1]*dt_sim;
        %
        % Interpolate the radiation IRFs onto the simulation
        % time-step size.
        %
        t_irfW=[0:irlim-1]*dt_irf; nt_irf=floor(t_irfW(end)/dt_sim)+1;
        t_irf=[0:nt_irf-1]*dt_sim;
        Kjk=zeros(nt_irf,nmodes,nmodes);
        for j=1:nmodes
            for k=1:nmodes
                switch Interpolation
                    case 'linear'
                        Kjk(:,j,k)=interp1(t_irfW,Kjk_W(:,j,k),t_irf,'linear','extrap');
                    case 'spline'
                        Kjk(:,j,k)=interp1(t_irfW,Kjk_W(:,j,k),t_irf,'spline','extrap');
                    otherwise
                        error('Interpolation must be set to either linear or spline')
                end
            end
        end
        %
        %  There is no wave forcing.
        %
        FjD=zeros(nmodes,nt_sim); zeta=zeros(nt_sim,1);
        %
    otherwise
        error('DecayTest must = Yes or No ')
end
