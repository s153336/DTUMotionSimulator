%%  DTUMotionSimulator
%
%  A Matlab package for simulating the linear or weakly nonlinear response 
%  of a floating marine structure to ocean waves.  This file is
%  self-documenting by using the "Publish" feature of Matlab.  A
%  pre-compiled version of the documentation is in ./html/.  
%
%% Description
%
% This matlab script solves the equations of motion in the time-domain to
% simulate a floating structure interacting with a prescribed ocean wave 
% elevation time-series specified at the origin of the body coordinates.  
%
% The hydrodynamic Frequency Response Functions (FRFs), inertia and 
% hydrostatic coefficients for the body are read in from WAMIT output files
% and used together with an input wave elevation time series to integrate
% the equations forward in time using the classical explicit 
% Runge-Kutta (4,4) scheme. External linear stiffness and damping matrices
% can be prescribed via the input file, as can the quadratic damping 
% matrix. Other forms of nonlinear forcing require the preparation of two 
% functions providing user-defined external forcing and constraints. 
%
% Some examples can be found in the 'examples' folder.  
%
%
%  Author/Contact:  Harry B. Bingham
%                   Dept. of Mechanical Engineering
%                   Technical University of Denmark
%                   hbb@mek.dtu.dk
%
%  Release:  1.3, Jan. 20, 2020
%
%% Download and Installation
% 
% The package repository is hosted at: 
%            
%       https://gitlab.gbar.dtu.dk/oceanwave3d/DTUMotionSimulator
%
% Go to 'Repository' and download the package archive. Unpack the archive
% at a convenient location on your machine. 
%
% 
%% Licence and acknowledgment
%
% DTUMotionSimulator is distributed under the GNU General Public License 
% (See the file LICENSE).  It is based on the the mildly-nonlinear
% extensions to a linearized potential flow analysis described in:  
%
% Bingham, H.B. (2000) "A hybrid Boussinesq-panel method for predicting 
%               the motions of a moored ship", Coastal Engineering, 40:1
%               pp. 21--38.  
%
% If you find the tool useful, then please acknowledge the software in your
% publications.  
%
%% Set-Up and Running a Simulation
%
% # Add the folder where you installed the package to your Matlab path 
% using pathtool.
% # Set up your test case in its own folder by computing the inertia
% matrix, hydrostatic matrix, radiation added mass and damping matrices and
% the wave exciting force coefficient vector (i.e. the Frequency-Response 
% Functions or FRFs) for your geometry.  The code currently supports 
% output files from WAMIT or TiMIT, but can easily be modified to support 
% other codes.  Ensure that the FRFs are computed on a uniform grid of
% frequencies covering the entire range of significant response including 
% the zero and infinity frequency limits. These limits should either both 
% appear first in the list of frequencies, or they should run from zero to 
% f_max followed by infinity as the last value.  When using WAMIT, either
% the Haskind relations or the diffraction exciting forces can be used.  
% # Create the run control input file DTUMotionSimulatorRunParameters.m 
% as described below (see also the examples under ./examples).  
% # Create the incident wave file named in the run control input file in
% the format shown below.  
% # Run the simulation by typing 'DTUMotionSimulator' on the command
% line.  (Ensure that you have navigated to the test case folder.)
% # Some default plotting and post-processing of the results is done at the
% end of this file, but this should be modified/extended as needed by the
% user.  
%
% The incident wave elevation file should have the following format:  
%
% * Header line
% * U, dt, nbeta    <- Forward speed, time step size, number of wave 
%                      heading angles
% * beta(1:nbeta)   <- Wave heading angles
% * x_0, y_0        <- Location of the signal(s) in the body-fixed 
%                      coordinates
% * zeta(1,1:nbeta) <- Wave elevation from each heading angle at time step 1
% * zeta(2,1:nbeta) <- Wave elevation from each heading angle at time step 2
% *      .
% *      .
% *      .
% 
% The number of zeta values determines the number of time-steps in the
% simulation.  
%
%% Main Program
%
% The 'clear all' line below can be commented out if multiple runs are 
% being processed and workspace variables need to be held on to. In this 
% case however, be careful not to define any of the global variables except
% via the input files. 
%
clear all; 
%
close all
%
% We pass the info needed by the dfdt routine via global variables.
%
global minv bjk bjkSqrt bjk2 cjk Kjk FjD memory nmodes dt_sim nt_irf ...
    FixedModes ulen grav rho nt_sim
%
% Preliminary I/O and initial set up.
%
% Initialize some default parameters which may or may not be set by the 
% main input file.  
%
FixedModes=[]; FreeModes=[]; Profile='off'; DecayTest='No'; U=0; 
gdfOrder='Low'; nHead=1; newmds=0; ialtfrc=1; FSPmodes=[]; ErrorFlag=0;
nbody=1; SoftSprings='off'; scalef=1; Interpolation='spline'; 
Integration='simpson'; IPEROUT=1;
%
switch Profile
    case 'on'
        profile on
end
%%
%
% First read the run control file which should set the following 
% parameters. Optional parameters can be left out if the defaults set above
% are correct for your case. 
% 
% * dt_sim -         The simulation time-step size.
% * scalef -         Optional. A scale factor for the output.
% * rho -            The fluid density.
% * FRFtype -        'WAMIT' or 'TiMIT'.
% * FRFfname -       The WAMIT numeric output file name if FRFtype='WAMIT'.
% * nHead -          The number of header lines in the WAMIT numeric output files.  
% * newmds -         Optional. The number of generalized modes for the structure.  
% * imode(1:nmodes)- 0 or 1 to indicate that a particular mode is fixed or free.  
%                    nmodes= total number of degrees of freedom.
% * nbody            Optional. The number of bodies in the WAMIT run.
% * ialtfrc -        Optional. 1 or 2 to indicate the type of WAMIT .frc file used.
% * IPEROUT -        Optional. 1 or 2 to indicate whether the WAMIT numeric
%                    output is in terms of wave period T or radian
%                    frequency omega. Default=1, period. 
%
% * GDFfname -       The WAMIT gdf file name if FRFtype='WAMIT'.
% * gdfOrder -       Optional. 'High' or 'Low' order geometric description if type='WAMIT.
%
% * zeta0file -      The incident wave file name if DecayTest='No'.
%
% * B0_external -    Optional external linear damping coefficient matrix.
% * C0_external -    Optional external linear stiffness coefficient matrix.
% * bjk2        -    Optional external quadratic damping coefficient matrix.
% * SoftSprings -    Optional. 'on' adds soft springs in surge, sway and yaw to hold
%                    the mean horizontal position of the body near zero. 
% * Interpolation -  Optional. 'linear' or 'spline', the IRF and FRF 
%                    interpolation scheme, set to spline by default. 
% * Integration -    Optional. 'trapezium' (2nd-order) or 'simpson'
%                              (4th-order) convolution integral
%                              integration scheme. Default -> simpson.
% * DecayTest -      Optional. Include set to 'Yes' for a decay test (no incident wave).
% * nt_sim -         Optional. The number of time-steps when DecayTest='Yes'.
% * x0(1:nmodes)-    Optional. The initial displacement of the body in nmodes>=6 degrees 
%                    of freedom (angles in degrees) for a decay test.    
%
%% 
%
% Read in the run paramters
%
DTUMotionSimulatorRunParameters
%
%
% Get the body geometry, inertia, hydrostatic, hydrodynamic coefficients 
% and possibly any external mass, damping and stiffness matrices.  
% The incident wave elevation is also read in, and this determines the
% simulation time step size dt_sim and it's length nt_sim.  The diffraction
% exciting force is computed in the frequency domain by multiplication of
% the diffraction FRF and the Fourier coefficients of the wave elevation;
% then inverse FFT'ed to get FjD(t,1:nmodes).  The Radiation IRFs, 
% Kjk(t,1:nmodes,1:nmodes), are computed from the cosine transform of the 
% damping coefficients; and interpolated onto the simulation 
% time-step size.  The infinite frequency added mass coefficients ajk 
% are added to the inertia coefficient msmtrx and the matrix is inverted 
% to give minv.  
%
%
ReadInput
%
% If external damping and stiffness matrices are not defined in the input
% file then initialize them. Note that these are added to any possible
% external stiffness and damping matrices which are defined by the WAMIT
% input files. If they do exist, then non-dimensionalize them here. As
% usual, if generalized/FSP modes are used then ULEN should be 1. 
%
if exist('B0_external','var')
    B0_external=B0_external/(rho*sqrt(grav/ulen)*ulen^3);
    B0_external(1:3,4:nmodes)=B0_external(1:3,4:nmodes)/ulen;
    B0_external(4:nmodes,1:3)=B0_external(4:nmodes,1:3)/ulen;
    B0_external(4:nmodes,4:nmodes)=B0_external(4:nmodes,4:nmodes)/ulen^2;
    display('External damping coefficients read from the input file')
else
    B0_external=zeros(nmodes); 
end
if exist('C0_external','var')
    C0_external=C0_external/(rho*grav*ulen^2);
    C0_external(1:3,4:nmodes)=C0_external(1:3,4:nmodes)/ulen;
    C0_external(4:nmodes,1:3)=C0_external(4:nmodes,1:3)/ulen;
    C0_external(4:nmodes,4:nmodes)=C0_external(4:nmodes,4:nmodes)/ulen^2;
    display('External stiffness coefficients read from the input file')
else
    C0_external=zeros(nmodes); 
end
bjk=bjk+B0_external;
cjk=cjk+C0_external;
%
% Initialize (if not already loaded) and non-dimensionalize the sqrt(v)
% and v^2 damping coefficients.  bjkSqrt is assumed to be for FSP surface 
% modes and hence the entire matrix is normalized as such.  If this is not 
% the case, then ulen should be set to 1.  
% Similarly for the bjk2 coefficients, if there are generalized modes then
% ulen should be set to 1. 
%
if length(bjkSqrt)~=nmodes, bjkSqrt=zeros(nmodes,nmodes); end; 
bjkSqrt=rho*bjkSqrt/ulen^4;
if length(bjk2)~=nmodes, bjk2=zeros(nmodes,nmodes); end; 
bjk2=bjk2/(rho* ulen^2);
bjk2(1:3,4:nmodes)=bjk2(1:3,4:nmodes)/ulen;
bjk2(4:nmodes,1:3)=bjk2(4:nmodes,1:3)/ulen;
bjk2(4:nmodes,4:nmodes)=bjk2(4:nmodes,4:nmodes)/ulen^2;
%
% Initialization call to the external force and constraint functions.
%
ExternalForces([],[],[],[],0);
ExternalConstraints([],[],[],0);
%
% We now have all the coefficients we need to solve the equations of motion
% and run the simulation.
%
% Initialize the body position and velocities vector, ordered:  
%
% x(1:nmodes,1:nt_sim)          = body velocities
% x(nmodes+1:2*nmodes,1:nt_sim) = body positions
%
x=zeros(2*nmodes,nt_sim);
x(nmodes+1:2*nmodes,1)=x0;
memory=zeros(nmodes);      % Initialize the memory convolution integrals
%
% Add the K_jk(0) contribution to the b_jk matrix to be applied to the
% current time-step value of the solution. 
%
bjk_ext=bjk; % external damping coefficient
for k=1:nmodes
    for j=1:nmodes
        bjk(j,k)=bjk_ext(j,k)+1/2*dt_sim*Kjk(1,j,k); % re-assigned below if Simpson's rule is used
    end
end
%
%
switch SoftSprings
    case 'on'
        %
        % Add soft springs in surge, sway and yaw to hold the mean position fixed.
        % The non-dimensional resonant frequency introduced here is set to 0.01
        % which is assumed to be below any frequencies of physical interest.
        %
        cjk(1,1)=cjk(1,1)+.01^2*(msmtrx(1,1)+ajk(1,1));
        cjk(2,2)=cjk(2,2)+.01^2*(msmtrx(2,2)+ajk(2,2));
        cjk(6,6)=cjk(6,6)+.01^2*(msmtrx(6,6)+ajk(6,6));
        display('Adding soft springs in surge, sway and yaw.')
end
%
% Roll damping coefficient, roughly:  rho vol B^2 sqrt{2g/B} 0.02.  
%
% Begin time-stepping the Equations of Motion
% 
disp(['Starting a simulation with ',num2str(nmodes),' degrees of freedom']);
disp(['  of which modes: ',num2str(FreeModes),' are considered free.']);
%
tic;
for it=2:nt_sim
    it0=it-1; t0=(it0-1)*dt_sim; % The old time-step
    t=(it-1)*dt_sim;             % The new time-step
    %
    % Compute the memory effects from the radiation problem by evaluating 
    % the convolution integrals for each mode combination.  The trapezoid 
    % rule is used for the integration.  
    %
    % Convolve up to the new time-step.
    %
    cstop=max(1,it-nt_irf+1); % The limit of the convolution integral
    for j=FreeModes
        for k=FreeModes
            switch Integration
                case 'trapezium'
                    memory(j,k)=1/2*dt_sim*(  x(k,cstop)*Kjk(nt_irf,j,k)+...
                                            2*x(k,cstop+1:1:it-1)*Kjk(it-cstop:-1:2,j,k)); % For an integration with N+1 equally spaced points corresponding to N panels, the trapezium rule approximation is: \int_a^b f(x)dx ~= 1/2*deltax*[f(x_1)+2{f(x_2)+f(x_3)+...+f(x_N)}+f(x_(N+1))]
                case 'simpson'
                    if it==2 % use trapezium rule
                        memory(j,k)=1/2*dt_sim*(  x(k,cstop)*Kjk(nt_irf,j,k)+...
                                                2*x(k,cstop+1:1:it-1)*Kjk(it-cstop:-1:2,j,k));
                    elseif rem(it-cstop,2)<=1e-6 % odd number of points, use normal Simpson's rule
                        memory(j,k)=1/3*dt_sim*(  x(k,cstop)*Kjk(nt_irf,j,k)+...
                                                4*x(k,cstop+1:2:it-1)*Kjk(it-cstop:  -2:2,j,k)+...
                                                2*x(k,cstop+2:2:it-2)*Kjk(it-cstop-1:-2:3,j,k)); % For an integration with an odd number of equally spaced points, N+1, corresponding to an even number of panels, N, Simpson's rule is: \int_a^b f(x)dx ~= 1/3*deltax*[f(x_1)+4{f(x_2)+f(x_4)+...+f(x_(N-1))}+2{f(x_3)+f(x_5)+...+f(x_(N))}+f(x_(N+1))]
                    else % even number of points, use modified Simpson's rule
                        memory(j,k)=1/3*dt_sim*(  x(k,cstop)*Kjk(nt_irf,j,k)+...
                                                4*x(k,cstop+1:2:it-4)*Kjk(it-cstop:  -2:5,j,k)+...
                                                2*x(k,cstop+2:2:it-5)*Kjk(it-cstop-1:-2:6,j,k)+...
                                                  x(k,it-3)*Kjk(4,j,k)); % normal Simpson's rule
                        memory(j,k)=memory(j,k)+3/8*dt_sim*(  x(k,it-3)*Kjk(4,j,k)+...
                                                            3*x(k,it-2)*Kjk(3,j,k)+...
                                                            3*x(k,it-1)*Kjk(2,j,k)); % Simpson's 3/8 rule
                    end
            end
        end
    end
    %
    % Add the K_jk(0) contribution to the b_jk matrix for Simpson's rule case.
    %
    if strcmp(Integration,'simpson') && it~=2 % trapezium rule applies when it=2
        if rem(it-cstop,2)<=1e-6 % odd number of points, use normal Simpson's rule
            for k=1:nmodes
                for j=1:nmodes
                    bjk(j,k)=bjk_ext(j,k)+1/3*dt_sim*Kjk(1,j,k);
                end
            end
        else % even number of points, use modified Simpson's rule
            for k=1:nmodes
                for j=1:nmodes
                    bjk(j,k)=bjk_ext(j,k)+3/8*dt_sim*Kjk(1,j,k);
                end
            end
        end
    end
    %
    % Take an R-K(4,4) step
    %
    x(:,it)=RK44(x(:,it-1),it0,t0,dt_sim,@dfdt_eqmot);
    %
    % Call the external constraints routine
    %
    x(:,it)=ExternalConstraints(t,x(:,it),nmodes,1);
    %
    % Check for unstable motions
    %
    if max(abs(x(nmodes+1:2*nmodes,it)))>100
        warning('The simulation looks to be going unstable, exiting here.');
        ErrorFlag=1;
        return
    end
end
%
switch Profile
    case 'on'
        profile off
        profile viewer
end
cpu=toc
%
%% Save the motions data
%
save('DTUMotionSimulatorResults.mat','ulen','grav','rho','nmodes',...
    'FreeModes','FSPmodes','msmtrx','ajk','t_sim','zeta','x');
%
%
% Final post-processing call to the external force and constraints
% functions
%
ExternalForces([],[],[],[],2);
ExternalConstraints([],[],[],2);
%
%% Post processing
%
% This section is meant to provide some default plotting of the results,
% but should be modified to suit the individual needs of the user.  
%
% Plot the motions.  The motions are non-dimensionalized by the
% length-scale ulen and angles are in radians.  
% 
figure; 
nfree=length(FreeModes); 
for j=1:nfree
    m=FreeModes(j);
    subplot(round(nfree/2),2,j); plot(t_sim,x(nmodes+m,:));
    xlabel('t sqrt(g/L)'); ylabel(['x_',num2str(m),' (non-d)']); 
end
%
% Compute the RAOs as the ratio of the FFT of the motions to the FFT of 
% the incident wave elevation.  
%
domega=2*pi/(nt_sim*dt_sim); nft=(nt_sim+1)/2; omega=[0:nft-1]*domega;
cZeta=fft(zeta(:,1)); rao=zeros(nt_sim,nmodes);
for j=FreeModes
    rao(:,j)=fft(x(nmodes+j,:)')./cZeta;
end
%
% Define the interesting range of dimensional cyclic frequency to plot
%
xrange=[0. .5];
%
ffac=sqrt(grav/ulen)/(2*pi); 
figure; 
for j=1:nfree
    m=FreeModes(j);
    subplot(round(nfree/2),2,j); plot(ffac*omega,abs(rao(1:nft,m)));
    xlim(xrange);
    xlabel('f [Hz]'); ylabel(['|rao_',num2str(m),'| (non-d)']); 
end
%
% Compare the RAOs with the original WAMIT calculations
%
PlotRAO='No';
switch PlotRAO
    case 'Yes'
        % Read in the .4 file WAMIT calcs.
        %
        fname=strcat(FRFfname,'.4'); Order='TM';
        [beta,T,raoW,raoPhase,raoR,raoI]=...
            ReadWAMIT234file(fname,nHead,Order);
        %
        % Plot the free modes.
        %
        for m=FreeModes
            figure;
            subplot(1,2,1);
            plot(1./T,raoW(:,m),...
                ffac*omega,abs(rao(1:nft,m)));
            xlim(xrange); xlabel('f [Hz]'); ylabel(['|RAO_',num2str(m),'|']);
            legend('WAMIT','T-D simulation'); grid on;
            subplot(1,2,2);
            plot(1./T,raoPhase(:,m),...
                ffac*omega,180/pi*atan2(imag(rao(1:nft,m)),real(rao(1:nft,m))));
            axis([xrange -180 180]); xlabel('f [Hz]'); ylabel(['RAO_',num2str(m),' phase']);
            legend('WAMIT','T-D simulation'); grid on;
        end
end
