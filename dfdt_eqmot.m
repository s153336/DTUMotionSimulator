function df = dfdt_eqmot(f,it,t,stage)
%
% dfdt routine for solving the equations of motion in the time-domain.  
%
% We pass the coefficients for the body via global variables.
%
global minv bjk bjkSqrt bjk2 cjk Kjk FjD memory nmodes dt_sim nt_irf ...
    FixedModes
%
% it is the index of the old time step unless we're at stage 4
%
% The radiation and diffraction forcing 
%
switch stage
    case {1, 4} % Whole time-step stages
        FD=FjD(:,it);
    case {2 , 3} % Mid-step stages at t0+dt/2
        % Linear interpolation of the diffraction forcing
        FD=.5*(FjD(:,it)+FjD(:,it+1));
end
Fext=ExternalForces(t,f,nmodes,stage,1);
%
% Sum up the radiation + hydrostatic forcing over all modes
%
for j=1:nmodes
    FR(j,1)=sum(memory(j,:)) + bjk(j,:)*f(1:nmodes,1) ...
        + sum(bjk2(j,:).*abs(f(1:nmodes,1)').*f(1:nmodes,1)') ...
        + sum(sqrt(bjkSqrt(j,:).*abs(f(nmodes+1:2*nmodes,1)')).*sign(f(nmodes+1:2*nmodes,1)')) ...
        + cjk(j,:)*f(nmodes+1:2*nmodes,1);
end
%
% Build the rhs of the time-stepping equations
%
df(nmodes+1:2*nmodes,1)=f(1:nmodes,1);
df(1:nmodes,1)=minv*(FD-FR+Fext);
%
% Zero the forcing for all modes which are held fixed.  
%
df(FixedModes,1)=0; df(nmodes+FixedModes,1)=0;
%
end
